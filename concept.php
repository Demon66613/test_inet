<?php
interface KeyStorage
{
    public function getKey(): string;
}

class FileStorage implements KeyStorage
{
    public function __construct(array $config)
    {

    }
    public function getKey()
    {
        return 'key';
    }
}

class DBStorage implements KeyStorage
{
    public function __construct(array $config)
    {

    }
    public function getKey()
    {
        return 'key';
    }
}

class StorageFactory
{
    private $config = [
        'db' => ['connection string and others'],
        'file' => ['file_path']
    ];
    private $storage;
    public function __construct(string $type)
    {
        $this->storage = match ($type) {
            'db' => new DBStorage($this->config['db']),
            'file' => new FileStorage($this->config['file']),
            default => new DBStorage($this->config['db']),
        };
    }

    public function getStorage(): KeyStorage
    {
        return $this->storage;
    }
}

class Concept
{
    private $client;
    private $storage;
    public function __construct(KeyStorage $storage)
    {
        $this->client = new \GuzzleHttp\Client();
        $this->storage = $storage;
    }

    public function getUserData()
    {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }

    private function getSecretKey()
    {
        return $this->storage->getKey();
    }
}
$storage = (new StorageFactory('file'))->getStorage();
(new Concept($storage))->getUserData();