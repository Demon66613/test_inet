<?php

$array = [
    ['id' => 1, 'date' => "12.01.2020", 'name' => "test1"],
    ['id' => 2, 'date' => "02.05.2020", 'name' => "test2"],
    ['id' => 4, 'date' => "08.03.2020", 'name' => "test4"],
    ['id' => 1, 'date' => "22.01.2020", 'name' => "test1"],
    ['id' => 2, 'date' => "11.11.2020", 'name' => "test4"],
    ['id' => 3, 'date' => "06.06.2020", 'name' => "test3"],

];
// 1. unique

$uniqueArray = array_reduce($array, function ($carry, $item) {
    $key = $item['id'];
    if (!isset($carry[$key])) {
        $carry[$key] = $item;
    }
    return $carry;
}, array());

$uniqueArray = array_values($uniqueArray);

print_r($uniqueArray);

// 2. sort

usort($uniqueArray, function ($a, $b) {
    return strtotime($a['date']) < strtotime($b['date']) ? -1 : 1;
});

print_r($uniqueArray);

// 3. filter
$needleId = 1;

$filtered = array_filter($array, function ($item) use ($needleId) {
    return $item['id'] == $needleId;
});

print_r($filtered);

// 4. combine

$combinedArray = array_combine(array_column($uniqueArray, 'name'), array_column($uniqueArray, 'id'));

print_r($combinedArray);

// 5-6. SQL

$sql5 = "SELECT g.id, g.name 
FROM goods g JOIN goods_tags gt 
ON g.id = gt.goods_id GROUP BY g.id 
HAVING COUNT(DISTINCT gt.tag_id) = (SELECT COUNT(*) FROM tags)";

$sql6 = "SELECT department_id
FROM evaluations
WHERE gender = true
GROUP BY department_id
HAVING COUNT(*) > 0 AND NOT EXISTS (
  SELECT 1
  FROM evaluations
  WHERE gender = true AND value <= 5 AND evaluations.department_id = department_id
)";