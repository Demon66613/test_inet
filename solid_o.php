<?php

interface SomeAction
{
    public function getHandler(): string;
}

class SomeObject implements SomeAction
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getObjectName()
    {
    }

    public function getHandler(): string
    {
        return 'handle_' . $this->name;
    }
}

class SomeObjectsHandler
{
    public function __construct()
    {
    }

    public function handleObjects($objects): array
    {
        $handlers = [];
        foreach ($objects as $object) {
            $handlers[] = $object->getHandler();
        }

        return $handlers;
    }
}

$objects = [
    new SomeObject('object_1'),
    new SomeObject('object_2')
];

$soh = new SomeObjectsHandler();
$soh->handleObjects($objects);